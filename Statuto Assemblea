Statuto Dell’Assemblea Degli Studenti


TITOLO I - PRINCIPI  FONDAMENTALI

Art.1 : Principi fondamentali. L’Assemblea è formata da tutti gli studenti interni del Collegio e ne esprime la volontà.
Sono diritti originari ed irrinunciabili dell’Assemblea valutare l’andamento della vita del Collegio, intervenire con proposte ed osservazioni, sottoporre a discussione tutto ciò che può riguardare o interessare gli studenti, invitare gli studenti ad assumere determinati atteggiamenti.
Gli altri poteri dell’Assemblea nella vita del Collegio sono stabiliti dagli accordi di volta in volta raggiunti con la Direzione e l’Amministrazione del Collegio.


TITOLO II - RIUNIONI ED ATTI DELL’ASSEMBLEA

Art.2 : Riunioni dell’Assemblea. L’Assemblea si riunisce in via ordinaria quattro volte all’anno nei mesi di ottobre, di dicembre, di marzo e di maggio, previa convocazione del Presidente.
Trenta studenti o quattro componenti del Consiglio studentesco possono richiedere la convocazione straordinaria dell’Assemblea.
Nessuna riunione può aver luogo in giorni di vacanza, festivi o prefestivi.
Le riunioni ordinarie dell’Assemblea sono valide se è presente la metà più uno dei componenti. In caso di non raggiungimento del numero previsto, l’Assemblea può riunirsi in seconda convocazione valida se è presente un terzo più uno dei componenti.
Le riunioni straordinarie dell’Assemblea sono valide se è presente un terzo più uno dei componenti.

Art.3 : Modalità di convocazione dell’Assemblea. Gli avvisi di convocazione ordinaria e straordinaria vengono esposti nell’apposita bacheca, rispettivamente con almeno tre e un giorno di anticipo rispetto a quello previsto per la riunione.
Unitamente all’avviso di convocazione, viene esposto l’ordine del giorno.

Art.4 : Ordine del giorno. Gli argomenti da trattare nella riunione dell’Assemblea ordinaria di ottobre sono:
  1.  elezione della Presidenza e della Segreteria ;
  2.  eventuale integrazione delle Commissioni incomplete ;
  3.  raccolta delle candidature per l’elezione dei rappresentanti degli studenti in Consiglio di Amministrazione e elezione dei rappresentanti nel Consiglio Studentesco Intercollegiale.
Gli argomenti da trattare nella riunione dell’Assemblea ordinaria di marzo sono:
  1. relazione della Commissione Concorso e Revisione ;
  2. resoconto sulle attività svolte dalle Commissioni nel primo semestre.
Gli argomenti da trattare nella riunione dell’Assemblea ordinaria di maggio sono:
  1.  elezione delle Commissioni ;
  2.  discussione della situazione inerente al concorso e alla revisione e ai criteri da suggerire ai membri della Commissione relativa.
Nel caso in cui un membro dell’Assemblea voglia aggiungere ulteriori argomenti, provvederà a rilevare l’interesse per gli stessi da parte di almeno trenta studenti, membri dell’Assemblea, con una raccolta firme. In seguito, provvederà a contattare la Presidenza e Segreteria.
L’ordine del giorno di una riunione straordinaria dell’Assemblea, fissato dagli studenti che la richiedono, non può essere in alcun modo alterato.

Art.5 : Apertura e svolgimento dell’Assemblea. La riunione dell’Assemblea è dichiarata aperta dal Presidente.
Gli argomenti da trattare vengono affrontati secondo la loro collocazione nell’ordine del giorno. Tuttavia il Presidente può proporre di mutare l’ordine di trattazione e la proposta, in mancanza di opposizioni, si ritiene senz’altro accettata. Se sorgono opposizioni, il Presidente sottopone la questione al voto dell’Assemblea.

Art.6 : Aggiornamento dell’Assemblea. Qualora risulti impossibile completare in un unica riunione la trattazione degli argomenti in programma e in ogni caso dopo due ore, il Presidente propone di aggiornare la riunione a uno dei giorni seguenti. In mancanza di opposizioni, la proposta si ritiene senz’altro accettata.

Art.7 : Interventi in Assemblea. Durante la riunione si può intervenire dopo aver ottenuto la parola dal Presidente, il quale la concede secondo l'ordine di domanda di coloro che l’hanno richiesta. Possono prendere la parola anche persone che non fanno parte dell’Assemblea, purché gli studenti presenti acconsentano.
Gli interventi devono essere contenuti in tempi ragionevoli ed evitare ripetizioni di concetti già espressi.

Art.8 : Proposte di mozioni. Quando ritiene che un argomento sia stato sufficientemente discusso, il Presidente fissa un termine ultimo per richiedere la parola e, terminati gli interventi previsti, dichiara chiusa la discussione e invita i membri dell’Assemblea a presentare delle mozioni sul punto trattato.  Se sorgono opposizioni, il presidente chiede in proposito il voto dell’Assemblea.
Se non vi sono proposte di mozioni, il Presidente, qualora lo ritenga opportuno, incarica uno dei segretari di compilare un’apposita mozione che riassuma le principali considerazioni emerse durante gli interventi.

Art.9 :  Mozioni. Le mozioni, proposte e approvate nei modi stabiliti costituiscono la forma normale di pronunciamento dell’Assemblea. Le mozioni sono conservate in un apposito registro, unitamente allo Statuto e agli atti dell’Assemblea. Tale registro è conservato da un segretario ed è liberamente consultabile da chiunque.
Le mozioni sono proposte per iscritto su appositi fogli conservati da un segretario.
Il presidente, se lo ritiene necessario per la chiarezza del pronunciamento dell’Assemblea, può strutturare e far votare per punti una mozione proposta come unitaria.
Art.9-bis: Proposte e mozioni
Tutte le proposte di modifica dello Statuto dovranno essere presentate in bacheca entro 3 gg universitari prima dell’Assemblea e all’email di Segreteria.
La proposta sarà valutata in sede deliberante in segreteria; l’esito sarà comunicato in bacheca al fine di eventuali ricorsi.

Art.10 : Approvazione delle mozioni. Quando tutte le mozioni sono state raccolte e lette ai presenti, il Presidente apre un dibattito di cui fissa i tempi, limitato alle dichiarazioni di voto.
Una mozione o i singoli punti di una mozione si intendono approvati quando si dichiara favorevole la maggioranza assoluta dei votanti. Sono considerati votanti coloro che esprimono voto favorevole e i contrari.
La stessa maggioranza, ove non ne sia espressamente prevista un’altra, è richiesta per ogni decisione dell’Assemblea.
Il Presidente è moderatore. (art.15)

Art.11 : Validità ed efficacia delle mozioni. Le mozioni in tutto o in parte contraddittorie approvate nella stessa riunione dell’Assemblea si annullano a vicenda. Sono nulle anche le mozioni contrastanti con norme inderogabili del presente Statuto. La nullità è dichiarata dal presidente su segnalazione di chiunque.
Qualora vengano approvate più mozioni con richieste o proposte analoghe, ha valore soltanto la mozione che ha contenuto più esteso.
Quando è rivolta ad organi dell’assemblea la mozione è immediatamente vincolante.

Art.12 : Mancato adempimento delle mozioni. Ogni studente può chiedere un voto di censura contro gli organi dell’assemblea per il mancato rispetto degli obblighi imposti da una mozione.
La censura non implica, in ogni caso, decadenza dalla carica.

Art.13 : Fondo dell’Assemblea. L’Assemblea ha a propria disposizione un fondo che viene utilizzato per sostenere le attività delle Commissioni.
Una quota degli utili delle Commissioni confluisce nel fondo dell’Assemblea.
L’Assemblea gestisce il fondo direttamente o tramite il Consiglio studentesco.


TITOLO III - ORGANI DELL’ ASSEMBLEA

Art.14 : Organi dell’Assemblea. Sono organi dell’Assemblea il Presidente, i Segretari, il Consiglio Studentesco, le Commissioni ordinarie e i rappresentanti degli studenti in Consiglio di Amministrazione se membri dell’Assemblea.
Sono organi temporanei le Commissioni straordinarie e le singole persone incaricate dall’Assemblea o dal Consiglio Studentesco di svolgere compiti particolari.
Il lavoro degli organi dell’Assemblea è gratuito. Sono fatti salvi diversi accordi tra la Direzione, l’Amministrazione e le Commissioni interessate riguardo singoli studenti.

SEZIONE I - Presidenza e Segreteria dell’Assemblea

Art. 15 : Presidente dell’Assemblea. Il Presidente rappresenta l’Assemblea e ne dirige le riunioni secondo le disposizioni del presente Statuto. Tutte le questioni procedurali non previste dallo Statuto sono risolte dal Presidente.
Il Presidente o uno dei Segretari partecipa all’attività della Commissione Concorso e Revisione.
Ogni impegno assunto dal Presidente a nome del Consiglio Studentesco va sottoposto a voto nella successiva riunione.
Il Presidente è moderatore (Art.10); ha ruolo di coordinamento della attività della Segreteria; si occupa in particolare dei rapporti con la Direzione e con i Capipiano.

Art. 16 : Segretari dell’Assemblea. Quattro Segretari coadiuvano il Presidente e partecipano a tutte le sue decisioni.
Ciascuno di essi svolge un ruolo specifico all'interno della segreteria: Responsabile Servizi Terzi (RST), Economo, Segretario e Responsabile Comunicazione.
Il RST si occupa dei rapporti con l’Amministrazione, con la ditta di pulizie e con la Commissione Mensa e Bar per il servizio di ristorazione.
L’Economo si occupa del bilancio dell’Assemblea, del Consiglio Studentesco e dei documenti delle Commissioni, prepara le sedute del Consiglio Studentesco predisponendo ogni documento necessario.
Il Segretario si occupa della verbalizzazione di Assemblea e Consiglio e di visionare le Relazioni sulla gestione delle Commissioni in collaborazione con l’Economo.
Il Responsabile Comunicazione si occupa della convocazione dell’Assemblea, di tutte le comunicazioni agli studenti, del NAS e dei sondaggi; cura la conservazione e la diffusione dei moduli dell’Assemblea (mozioni e simili) e li raccoglie dagli Studenti, quando essi li presentano.
Nel caso di impedimento del Presidente le sue funzioni sono assunte immediatamente dal Vicepresidente, nominato secondo quanto previsto dall'Art. 17.

Art.16-bis: Segretari dell’assemblea
I segretari hanno facoltà di valutare le eventuali proposte di modifica dello St. (art 9bis) operando in sede deliberante.

Art. 17 : Elezione del Presidente e dei Segretari dell’Assemblea.
Presidente e Segretari sono eletti nella riunione ordinaria di ottobre. Affinché l’elezione sia valida tutti i candidati devono essere presenti, salvo grave e motivata causa, e confermare la propria disponibilità. Per il Presidente affinché l’elezione sia valida i candidati (anche se uno) devono aver affisso in bacheca la lettera di candidatura entro i 2 (due) giorni antecedenti la data dell’Assemblea di ottobre. In tale lettera bisogna indicare un elenco di Segretari candidati con il rispettivo incarico.
Uno studente può candidarsi singolarmente come Segretario affiggendo la lettera in bacheca entro i 2 (due) giorni antecedenti la data dell’Assemblea di ottobre, specificando il ruolo che desidera ricoprire.
È possibile essere candidato Segretario sia singolarmente che in uno o più elenchi.

Il Presidente viene eletto secondo le seguenti modalità:
  a) In caso di 2 candidati:
    → Si effettua una votazione segreta (vedi procedura nel dettaglio qui sotto); viene eletto chi riceve la maggioranza assoluta dei votanti.
    → In caso di eventuale pareggio tra i due candidati, si può ricorrere a due procedure (da decidere): sorteggio o presentazione in Assemblea della proposta “Il consiglio si riunisce nella stanza affianco e decide”.

    Procedura a brevi linee:
      ◦ Istituzione di due seggi – divisi per cognome A-L e M-Z;
      ◦ Utilizzo di schede siglate dal presidente uscente;
      ◦ Registrazione dei votanti, tramite firma del singolo, al momento della consegna della scheda;
      ◦ In funzione di scrutinatori saranno predisposti 2 segretari uscenti.

  b) In caso di 3 o più candidati:
  Presi, come situazione limite, tre candidati (A, B e C) vengono analizzate e risolte le diverse situazioni:
    1) Uno dei 3 candidati riceve 50% + 1 dei voti, allora risulta eletto presidente;
    2) Se abbiamo uno scarto maggiore o uguale dell’1% approssimato per eccesso (1 voto se i votanti sono minori di 100, 2 voti se i votanti sono maggiori di 100) allora si procede al ballottaggio tra i due che hanno ricevuto più voti (in questo caso quindi tra A e B).
    3) Se si presenta una situazione di ex aequo tra due che hanno ricevuto voto minimo (per es. A 48 B 42 C 42) allora si riprende situazione vista in precedenza (vedi caso due candidati – pareggio): sorteggio o adunanza consiglio.
     
I Segretari vengono eletti, per alzata di mano con voto palese, secondo le seguenti modalità:
  a) In caso di nessun candidato singolo: 
  Votazione in blocco della Segreteria proposta dal presidente eletto. Viene considerata eletta la segreteria con l'approvazione del 50% + 1 dei votanti.

  b) In caso di almeno un candidato singolo:
  → Votazione in blocco della Segreteria proposta dal presidente eletto. Viene considerata eletta la segreteria con l'approvazione dei 2/3 dei votanti.
  → In caso contrario si procede, per ogni incarico previsto dallo Statuto, all'elezione dei singoli candidati.
  Sono eleggibili coloro che abbiano affisso la lettera di candidatura singola specificando tale incarico e colui che è indicato a tale ruolo nell'elenco del presidente eletto.

In caso di mancata elezione di uno o più segretari è possibile presentare la propria candidatura prima del punto succesivo all'o.d.g.

Il Vicepresidente è eletto con votazione palese dall’Assemblea immediatamente dopo l’elezione della Segreteria, tra i componenti della stessa. Ogni studente avrà a disposizione una preferenza, risulta eletto Vicepresidente il Segretario che ottiene il maggior numero di voti.

Art. 17-bis: Validità schede elettorali. In caso di voto segreto, la scheda bianca e la scheda nulla sono equivalenti, ovvero contribuiscono entrambe al conteggio dei votanti. È considerato astenuto (non votante) ogni membro dell'assemblea che non si registri come previsto dall'Art.17.
Prima dell'inizio della votazione il presidente uscente è tenuto ad illustrare le modalità di voto ai presenti.

Art. 18 : Dimissioni del Presidente e dei Segretari dell’Assemblea. Se il Presidente si dimette prima del termine naturale del suo mandato, assume la carica di Presidente il Vicepresidente.
Se sono dimissionari tanto il Presidente quanto i Segretari, sono prorogati i poteri del Presidente in carica per il periodo strettamente necessario a far svolgere nuove elezioni.
I nuovi eletti rimangono in carica fino alla scadenza naturale del mandato dei dimissionari.

SEZIONE II - Consiglio Studentesco

Art.19 : Composizione del Consiglio Studentesco. Il Consiglio Studentesco è formato dal Presidente dell’Assemblea, dai Segretari, dai Presidenti delle Commissioni ordinarie, dai Presidenti dei Gruppi riconosciuti, dai rappresentanti in Consiglio Studentesco Intercollegiale, dai rappresentanti in Consiglio di Amministrazione se membri dell’Assemblea, dai Capipiano e dai rappresentanti dell’edificio M e dell’edificio C. Tutti i partecipanti hanno eguale diritto di voto. Ogni persona non può rappresentare più di una componente del Consiglio, neanche su delega. Se una persona rappresenta più di una componente del Consiglio deve nominare, entro la prima riunione del Consiglio, un suo rappresentante in esso, che non può cambiare e non può delegare.

Art.20 : Competenze del Consiglio Studentesco. Il Consiglio Studentesco:
  • promuove e coordina le attività organizzate all’interno del Collegio;
  • approva tutte le iniziative di carattere culturale e ricreativo proposte dalle Commissioni o da singoli studenti;
  • approva il bilancio preventivo e consuntivo delle Commissioni;
  • gestisce il fondo dell’Assemblea;
  • costituisce per la Direzione del Collegio il riferimento cui rivolgersi per informazioni o chiarimenti.

Art.21 : Presidenza del Consiglio Studentesco. La presidenza del Consiglio Studentesco spetta al Presidente dell’Assemblea, che ne dirige le riunioni e risolve tutte le questioni procedurali non previste dallo Statuto.
In caso di assenza, il Presidente dell’Assemblea delega in sua vece uno dei Segretari.

Art.22 : Riunioni del Consiglio Studentesco. Il Consiglio Studentesco si riunisce in via ordinaria ogni mese, previa convocazione del Presidente.
Il Presidente è tenuto inoltre a convocare senza ritardo, in via straordinaria, il Consiglio Studentesco qualora ne sia fatta domanda da uno o più componenti dello stesso Consiglio.
Tutti gli studenti del Collegio possono partecipare alle riunioni del Consiglio Studentesco, senza diritto di voto.

Art.23 : Convocazione del Consiglio Studentesco. Gli avvisi di convocazione ordinaria o straordinaria vengono esposti nell’apposita bacheca, rispettivamente con almeno tre o un giorno di anticipo rispetto a quello previsto per la riunione.
Unitamente all’avviso di convocazione viene esposto l’elenco degli argomenti da trattare, con carattere vincolante.

Art.24 : Validità delle decisioni del Consiglio Studentesco. Per la validità delle decisioni del Consiglio Studentesco è necessaria la presenza dei due terzi dei componenti, arrotondati per difetto. Se tale quota non viene raggiunta, la riunione va aggiornata a data successiva.
Ciascun Presidente di una Commissione ordinaria, in caso di assenza, può farsi sostituire da un altro componente della Commissione stessa, eccetto il caso in cui quest’ultimo rappresenti già una componente del Consiglio Studentesco.
Le decisioni del Consiglio Studentesco sono prese a maggioranza dei componenti presenti. In caso di parità tra voti favorevoli e contrari, è decisivo il voto del Presidente del Consiglio Studentesco.

Art.25 : Pubblicità delle decisioni del Consiglio Studentesco. Le decisioni prese in Consiglio Studentesco devono essere comunicate nell’apposita bacheca entro due giorni dalla riunione.
Dal giorno dell’affissione in bacheca decorre il termine di sei giorni non festivi o di vacanza per richiedere la convocazione straordinaria dell’Assemblea.

SEZIONE III – Commissioni e Gruppi riconosciuti

Art.26 : Ruolo delle Commissioni. I compiti assunti dall’Assemblea nella vita del Collegio sono svolti da apposite Commissioni, alle quali viene riconosciuta la più ampia autonomia organizzativa.
Le questioni non previste dallo Statuto sono disciplinate dal Regolamento delle Commissioni.

Art.27 : Creazione delle Commissioni. Nuove Commissioni ordinarie e tutte le Commissioni straordinarie sono create con mozione dell’Assemblea che ne specifica i compiti.
Con le stesse modalità, l’Assemblea può affidare compiti particolari a singole persone.

Art. 28 Riconoscimento dei Gruppi. I Gruppi possono essere riconosciuti in Consiglio Studentesco facendone richiesta, indirizzata al Presidente dell’Assemblea, entro il 15 novembre di ogni anno. La richiesta deve essere redatta sul modulo disponibile presso la Segreteria dell’Assemblea. Il riconoscimento dei Gruppi e la loro rimozione vengono deliberati con la maggioranza assoluta dei componenti del Consiglio Studentesco, nella prima riunione utile. Il riconoscimento dei Gruppi comporta il loro inserimento nel Consiglio Studentesco. I Gruppi per essere riconosciuti, oltre alla richiesta anzidetta, devono:
  • Eleggere un Presidente, un Segretario e un Economo (se avanzano richiesta di dotarsi di un portafoglio).
  • Perseguire finalità utili alla collettività degli studenti del Collegio (la valutazione spetta al Consiglio Studentesco)
I Gruppi riconosciuti in Consiglio Studentesco rispettano il Regolamento delle Commissioni Ordinarie, eccetto i titoli I e II.
Un Gruppo attivo e riconosciuto da almeno un anno permane in Consiglio Studentesco finché esso non si sciolga dandone comunicazione al Presidente dell’Assemblea o finché il Consiglio Studentesco non ne disponga la rimozione dallo stesso.
Il presente articolo 28 non è applicabile ai gruppi retribuiti.

Art.29 : Riunioni delle Commissioni. Tutte le riunioni delle Commissioni sono pubbliche e il relativo avviso di convocazione è affisso in bacheca con opportuno anticipo.
Ad ogni riunione devono partecipare almeno i due terzi dei componenti, arrotondati per difetto. Il non raggiungimento della quota necessaria di commissari richiede l’aggiornamento della riunione a data successiva.
Le decisioni prese devono essere esposte in bacheca.

Art.30 : Cariche interne delle Commissioni. Ogni Commissione deve eleggere un Presidente, un Segretario e un Economo, quest’ultimo solo nelle Commissioni che devono presentare il bilancio.
Il Presidente rappresenta la Commissione nel Consiglio Studentesco, tiene i rapporti con la Direzione e convoca le riunioni periodiche della Commissione.
Il Segretario ha il compito di rendere pubbliche e facilmente conoscibili agli studenti del Collegio i verbali, le proposte e le decisioni della Commissione di cui fa parte.
L’Economo cura la contabilità della Commissione e mantiene i contatti con l’Amministrazione del Collegio.

Art.31 : Bilanci delle Commissioni. Quando hanno portato a termine il loro mandato, le Commissioni presentano al Consiglio Studentesco una relazione e un bilancio che vanno discussi e approvati.

SEZIONE IV - Rappresentanti in consiglio d’amministrazione

Art.32 : Rappresentanti in Consiglio d’Amministrazione. I Rappresentanti degli studenti in Consiglio d’Amministrazione portano in quest’organismo il parere degli studenti così com’è espresso nei pronunciamenti dell’Assemblea e del Consiglio Studentesco.
L’elezione dei due rappresentanti avviene secondo le modalità previste dallo Statuto del Collegio e dal regolamento per l’elezione dei Rappresentanti in Consiglio di Amministrazione, in accordo con le rappresentanze di tutte le residenze del Collegio.

SEZIONE V - Rappresentanti in Consiglio Studentesco Intercollegiale

Art.33 : Elezione Rappresentanti CSI. L’Assemblea degli Studenti, nella riunione di ottobre, elegge due Rappresentanti nel Consiglio Studentesco Intercollegiale. Ad essi è delegata la rappresentanza dell’Assemblea in seno a tale organo e dovranno riferire all’Assemblea sui lavori del Consiglio Studentesco Intercollegiale.
Nel caso in cui non vi siano candidati, il Presidente dell’Assemblea degli Studenti assumerà il ruolo di Rappresentante nel Consiglio Studentesco Intercollegiale. Il Presidente dell’Assemblea ha la facoltà di delegare la partecipazione alle riunioni del Consiglio Studentesco Intercollegiale ad uno studente facente parte dell’Assemblea degli Studenti.

TITOLO IV - REVISIONE DELLO STATUTO

Art.34 : Modifica dello Statuto. Le modificazioni, le integrazioni e la radicale sostituzione del presente Statuto sono discusse e votate nel Consiglio.
In consiglio va discusso e votato ogni singolo articolo della proposta di modifica e sono possibili emendamenti.
L’articolo o l’emendamento sono successivamente approvati in Consiglio se si dichiarano favorevoli i due terzi dei votanti.
In seguito vengono proposte in maniera dettagliata e affisse in bacheca, entro i due giorni precedenti la riunione, le varie modifiche, per essere votate in blocco alla seguente Assemblea.
Nel caso in cui non ci sia maggioranza dei 2/3 dell’Assemblea si va a presentazione punto per punto.
L’articolo o l’emendamento nuovamente sottoposti a votazione sono successivamente approvati se si dichiarano favorevoli i due terzi dei votanti.

Ultima modifica: 28 Maggio 2019
  Il Presidente dell’Assemblea
    Simone Pavarana
  I Segretari dell’Assemblea
    Alessandro Marcomini
    Davide Peressoni
    Giulio Carazzolo
    Marco Dassie