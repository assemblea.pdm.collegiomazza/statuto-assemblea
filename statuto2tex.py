#!/bin/python3
# -*- coding: utf-8 -*-

# © Rompicapuz (Davide Peressoni) 2018
# Made in Granducato

import sys,codecs,re

if len(sys.argv) == 1:
  print('Inserire il nome del file sorgente')
  exit()
file=sys.argv[1]

#Apro file sorgente
try:
  source=codecs.open(file, 'r','utf-8' )
except:
  print('Orpo, non riesco ad aprire il file', file)
  sys.exit(3)
    
#Apro file di destinazione
try:
  dest = codecs.open(file+'.tex', 'w','utf-8')
except:
  print('Orpo, non riesco a creare il file', file+'.tex')
  sys.exit(3)
  
#Header
dest.write('''\documentclass[a4paper,11pt]{article}
\pdfpagewidth\paperwidth
\pdfpageheight\paperheight
\\usepackage[italian]{babel}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage[margin=2cm]{geometry}
\\usepackage{lmodern,titlesec,xcolor,tabularx,hanging,calc,textcomp}
\\usepackage[hidelinks]{hyperref}
\\titleformat{\section}{\\normalfont\Large\\bfseries}{TITOLO \Roman{section}}{1em}{}
\\titleformat{\subsection}{\\normalfont\large\\bfseries}{SEZIONE \Roman{subsection}}{1em}{}
\\titleformat{\subsubsection}[runin]{\\bfseries}{}{}{}[]
\\newlength{\PARindent}
\\newlength{\initPARindent}
\\DeclareUnicodeCharacter{2192}{$\\rightarrow$}
\\begin{document}
{\centering\\fcolorbox{black}{gray!20}{\parbox{\\textwidth}{\Huge\centering\\textsc{~\\\\'''+source.readline()[:-1]+'\\vspace{0.2in}}}}}')

statuto=source.read()
statuto=re.sub(r'\n([ \t]+)([◦•·→]|(?:[0-9]|[a-z]|[A-Z]) {0,1}[\)\.]) *',r'\n\1\\setlength{\\initPARindent}{0pt-\\widthof{\2~}}\\hspace{\\initPARindent}\2~',statuto)
statuto=re.sub(r'\n([ \t]+)([^\n]+)',r'\n\\par\\setlength{\\PARindent}{\\widthof{\1}+1.5em}\\hangpara{\\PARindent}{0}\2\\par\\noindent',statuto)
statuto=re.sub('◦',r'\\textopenbullet{}',statuto)
statuto=re.sub('•',r'\\textbullet{}',statuto)
statuto=re.sub('·',r'\\textbullet{}',statuto)
for i in re.findall(r'(widthof{[ \t]*})',statuto):
  statuto=statuto.replace(i,re.sub(r'[ \t]','m',i))
statuto=re.sub('%','\%',statuto)
statuto=re.sub('–','-',statuto)
statuto=re.sub(' €','~€',statuto)
statuto=re.sub('€',r'\\texteuro{}',statuto)
statuto=re.sub(r'TITOLO[^-]*- ([^\n]*)',r'\\section{\1}',statuto)
statuto=re.sub(r'\n([0-9]*)\. ([^\n]*\.{0,1})',r'\\section*{\\refstepcounter{section}\1. \2}',statuto)
statuto=re.sub(r'SEZIONE[^-]*- ([^\n]*)',r'\\subsection{\1}',statuto)
statuto=re.sub(r'\nArt\.([0-9]+(?: bis){0,1}(?: ter){0,1})\.',r'\\subsubsection*{\\refstepcounter{subsubsection}Art€.\1. }\\label{subsubsec:\1}',statuto)
statuto=re.sub(r'\nArt\. {0,1}([0-9]+(?:-bis){0,1}(?:-ter){0,1}) {0,1}:{0,1} ([^\.\n]*)\.{0,1}\n{0,1}',r'\\subsubsection*{\\refstepcounter{subsubsection}Art€. \1 : \2\\\\}\\label{subsubsec:\1}',statuto)
statuto=re.sub(r'(Ultima modifica:[^\n]*)',r'~\\\\~\\\\\1\\\\~',statuto)
statuto=re.sub(r'\n.*\n(.*)\n.*\n(.*)\n(.*)\n(.*)\n(.*)\n{0,1}\Z',r'\\begin{tabularx}{\\textwidth}{Xc}~&\\textbf{Il Presidente dell’Assemblea}\\\\~&\1\\\\~&\\textbf{I Segretari dell’Assemblea}\\\\~&\2\\\\~&\3\\\\~&\4\\\\~&\5\\end{tabularx}',statuto)
statuto=re.sub(r'([^}])([Aa]rt.{0,1} {0,1}[0-9]+)(bis|ter)',r'\1\2\3',statuto)
statuto=re.sub(r'([^}])[Aa]rt.{0,1} {0,1}([0-9]+(?:[- ]bis){0,1}(?:[- ]ter){0,1})',r'\1\\hyperref[subsubsec:\2]{Art. \2}',statuto)
statuto=re.sub('€','',statuto) #gli euro facevano da distinzione fra gli articoli già tradotti e quelli no
statuto=re.sub(r'([^\n])(?:par){0}\n+([^\n\\])',r'\1\\\\\2',statuto)
statuto=re.sub('\n\n','\\\\\n',statuto)
dest.write(statuto)

dest.write('\end{document}')
